(function ($) {

  Drupal.behaviors.fastContentTypeUI = {
    attach: function(context) {

      var contentTypes = $.parseJSON($('#fast-content-type-ui-form input[name=fast_content_type_json]').val());
      var i = false;
      var tr = $('<tr></tr>');
      var td = $('<td></td>');
      var row = false;
      var cell = false;
      var cellContent = false;
      var machineName = $('<input type="text" class="machine-name form-text" size="40" />');
      var table = $('<table id="fast-content-type-ui-list"><thead><tr><td>Display Name</td><td>Machine Name</td></tr></thead><tbody></tbody></table>');
      var actionLinks = $('<ul id="fast-content-type-ui-action-links" class="action-links"></ul>');
      var addLink = $('<a href="#">Add content type</a>');
      var commentsActive = false;

      for (i in contentTypes) {
        if (typeof contentTypes[i].comment != 'undefined') {
          commentsActive = true;
          break;
        }
      }

      if (commentsActive) {
        table.find('thead tr').append('<td>Comment</td>');
      }

      if ($('#fast-content-type-ui-list').length > 0) {
        $('#fast-content-type-ui-list').remove();
      }

      if ($('#fast-content-type-ui-action-links').length > 0) {
        $('#fast-content-type-ui-action-links').remove();
      }

      actionLinks.insertBefore('#fast-content-type-ui-form');
      table.insertBefore('#fast-content-type-ui-form');

      for (i in contentTypes) {

        cellContent = $('<input type="text" class="display-name form-text" size="40" />')
          .val(contentTypes[i].name)
          .change(fastContentTypeUIrebuildJson);
        cell = td.clone().append(cellContent);
        row = tr.clone().append(cell);
        cell = td.clone().append('<i class="machine-name">' + contentTypes[i].type + '</i>');
        row.append(cell);

        if (commentsActive) {
          cellContent = $('<input type="checkbox" />').click(fastContentTypeUIcommentClick);
          if (contentTypes[i].comment == '2') {
            cellContent.attr('checked', 'checked');
          }
          cell = td.clone().append(cellContent);
          cellContent = $('<input type="hidden" class="comment" />')
            .val(contentTypes[i].comment)
            .change(fastContentTypeUIrebuildJson);
          cell.append(cellContent);
          row.append(cell);
        }

        row.appendTo(table.find('tbody'));

      }

      addLink.click(function() {

        displayName = $('<input type="text" class="display-name form-text" size="40" />')
          .change(fastContentTypeUIrebuildJson);
        cell = td.clone().append(displayName);
        row = tr.clone().append(cell);
        cell = td.clone().append(machineName.clone().change(fastContentTypeUIrebuildJson));
        row.append(cell);

        if (commentsActive) {
          cell = td.clone();
          cellContent = $('<input type="checkbox" />').click(fastContentTypeUIcommentClick);
          cell.append(cellContent);
          cellContent = $('<input type="hidden" class="comment" value="0" />').change(fastContentTypeUIrebuildJson);
          cell.append(cellContent);
          row.append(cell);
        }

        row.prependTo(table);

        return false;

      });
      $('<li></li>').append(addLink).appendTo(actionLinks);

    },
  };

  function fastContentTypeUIcommentClick() {
    if ($(this).attr('checked')) {
      $(this).siblings('.comment').val(2).change();
    }
    else {
      $(this).siblings('.comment').val(0).change();
    }
  }

  function fastContentTypeUIrebuildJson() {

    var i = false;
    var row = false;
    var machineName = false;
    var contentTypes = $.parseJSON($('#fast-content-type-ui-form input[name=fast_content_type_json]').val());
    var commentsActive = false;

    for (i in contentTypes) {
      if (typeof contentTypes[i].comment != 'undefined') {
        commentsActive = true;
        break;
      }
    }

    $('#fast-content-type-ui-list tbody tr').each(function(){

      row = $(this);
      machineName = row.find('.machine-name');

      if (machineName.is('i')) {
        machineName = machineName.text();
      }
      else {
        machineName = machineName.val();
      }

      if (machineName.length > 0) {

        if (typeof contentTypes[machineName] === 'undefined') {
          contentTypes[machineName] = {}
          contentTypes[machineName].type = machineName;
        }

        contentTypes[machineName].name = row.find('.display-name').val();

        if (commentsActive) {
          contentTypes[machineName].comment = row.find('.comment').val()
        }

      }

    });

    $('#fast-content-type-ui-form input[name=fast_content_type_json]').val(JSON.stringify(contentTypes));

  }

}) (jQuery);
